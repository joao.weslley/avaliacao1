#!/bin/bash

echo "Criando variaveis no shell script"

echo "É possível criar variáveis no shell script da seguinte forma:
"

echo "É possível criar usando =, por exemplo:
"

echo "V1 = 10
"

echo "Outra forma de se criar uma váriavel é usando o comando read, onde o user terá de digitar o valor da variável, por exemplo:
"

echo "read V2
"

echo "Por fim é também é possível criar uma variável utilizando \$[num], e isso permite a ela ser passada como parametro do script.
"
echo "Para acessá-las utiliza-se o $
"

echo "printf \"\$V1\""

echo "printf \"\$1\"
"

echo "A principal diferença entre pedir para o usuário digitar a variável, usando o read, é que isso irá parar o script até que o usuário passe o valor para a variável. Já deixá-lo como parametro irá acelerar a execução e torná-la mais automática e menos propensa a erros de entrada."
