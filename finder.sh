#!/bin/bash

printf 'Digite o nome de três diretórios...\n'
read d1 d2 d3

#movendo até o diretorio 1

cd $d1

diretorio=$(pwd)

printf "$diretorio\n\n"

printf "dir1.txt = $(ls $diretorio/*.txt | wc -l)\n"
printf "dir1.png = $(ls $diretorio/*.png | wc -l)\n"
printf "dir1.doc = $(ls $diretorio/*.doc | wc -l)\n"

cd ..

cd $d2

diretorio=$(pwd)

printf "$diretorio\n\n"

printf "dir2.txt = $(ls $diretorio/*.txt | wc -l)\n"
printf "dir2.png = $(ls $diretorio/*.png | wc -l)\n"
printf "dir2.doc = $(ls $diretorio/*.doc | wc -l)\n"

cd ..

cd $d3

diretorio=$(pwd)

printf "$diretorio\n\n"

printf "dir3.txt = $(ls $diretorio/*.txt | wc -l)\n"
printf "dir3.png = $(ls $diretorio/*.png | wc -l)\n"
printf "dir3.doc = $(ls $diretorio/*.doc | wc -l)\n"
