#!/bin/bash

clear
#1
printf 'O medo de cair não pode ser maior do que a paixão de voar\n'
sleep 1
#2
printf 'Seja o arco-íris da nuvem de alguem\n'
sleep 1
#3
printf 'Põs o seu melhor sorriso, ele irradia a luz de dentro de você\n'
sleep 1
#4
printf 'Tudo tem o seu tempo determinado, e há tempo para todo o propósito debaixo do céu\n'
sleep 1
#5
printf 'Não coloque limites no seus sonhos. Coloque Fé\n'
sleep 1
#6
printf 'Não importa a platéia, faça sempre o seu melhor\n'
sleep 1
#7
printf 'Um elogio sincero pode mudar o seu dia\n'
sleep 1
#8
printf 'Você nasceu pra vencer\n'
sleep 1
#9
printf 'Uma palavra gentil pode transformar o dia infeiro de alguém\n'
sleep 1
#10
printf 'Se chover amor eu te desejo uma tempestade.\n'
