#!/bin/bash

printf "\$USER - Exibe o user logado no shell\n"

printf "\$PWD - Exibe o diretorio atual onde o script está sendo executado\n"

printf "\$HOME - Exibe o diretorio home do user logado\n"

printf "\$SHELL - Exibe o shell usado\n"

printf "\$OSTYPE - Exibe o tipo do S.O.\n"

printf "\$RANDOM - Gera um número aleatório e printa na tela\n"

printf "\$DISPLAY - Exibe o nome do computador e o número do display\n"

printf "\$UID - Exibe o user-id do usuário que rodou o script\n"

printf "\$PATH - Exibe o Path do sistema atual\n"

printf "\$LANG - Exibe o idioma atual do sistema\n"
